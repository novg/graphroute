#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class FilterCalls;
//class QueryCalls;
class LoadLine;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void signalSendTableName(const QString &name);

private slots:
    void slotSettings();
    void slotAbout();

private:
    void readSettings();
    void writeSettings();
    bool createConnection();
    void createWidgets();


private:
    FilterCalls *filterCalls;
//    QueryCalls *queryCalls;
    LoadLine *loadLine;
    QString hostName;
    QString baseName;
    QString userName;
    QString password;
};

#endif // MAINWINDOW_H
