#include "loadline.h"
#include "qcustomplot.h"

#include <QBoxLayout>
#include <QComboBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QList>
#include <QDebug>

LoadLine::LoadLine(QWidget *parent) : AbstractCalls(parent)
{
    createActions();
    createWidgets();
    createConnect();

    connect(comboLine, SIGNAL(activated(QString)),
            SLOT(slotShowGraph(QString)));
    connect(actionSave, SIGNAL(triggered()), SLOT(slotSaveToFile()));
}

LoadLine::~LoadLine()
{

}

void LoadLine::slotShowGraph(const QString &line)
{
    QString strQuery;
    double countChannel = 10;
    double rangeChannel = 0;

    QString strFirstDateTime =
            firstDateTime->dateTime().toString("yyyy-MM-dd hh:mm");
    QString strLastDateTime =
            lastDateTime->dateTime().toString("yyyy-MM-dd hh:mm");

    if (line == "Спорт-отель") {
        countChannel = 10;
        rangeChannel = 13;
        strQuery = QString("SELECT datetime, duration FROM calls "
                           "WHERE datetime BETWEEN '%1' and '%2' AND "
                           "(numout LIKE '%796%' OR numout LIKE '%797%' "
                           "OR numout LIKE '%798%' OR numin LIKE '%796%' "
                           "OR numin LIKE '%797%' OR numin LIKE '%798%') "
                           "AND numout NOT LIKE '77%' AND numin NOT LIKE '77%' "
                           "ORDER BY datetime "
                           ";").arg(strFirstDateTime).arg(strLastDateTime);
    }
    else if (line == "Ericsson MD-110") {
        countChannel = 60;
        rangeChannel = 75;
        strQuery = QString("SELECT datetime, duration FROM calls "
                           "WHERE datetime BETWEEN '%1' and '%2' AND "
                           "(seg = 'I' AND numout LIKE '7%' OR dest = '584' "
                           "OR str2 LIKE '1%' AND seg != 'M') "
                           "ORDER BY datetime "
                           ";").arg(strFirstDateTime).arg(strLastDateTime);
    }
    else if (line == "Right-Fax") {
        countChannel = 4;
        rangeChannel = 7;
        strQuery = QString("SELECT datetime, duration FROM calls "
                           "WHERE datetime BETWEEN '%1' and '%2' AND "
                           "(numin IN ('73247','77766','77788','73100','73107', "
                           "'73209','73282','73321','73372','73687','73801', "
                           "'73809','73865','73893','73905','77785') "
                           "OR numout IN ('73247','77766','77788','73100', "
                           "'73107','73209','73282','73321','73372','73687', "
                           "'73801','73809','73865','73893','73905','77785')) "
                           "ORDER BY datetime "
                           ";").arg(strFirstDateTime).arg(strLastDateTime);
    }
    else if (line == "AXE-10 (Саяногорск)") {
        countChannel = 76;
        rangeChannel = 95;
        strQuery = QString("SELECT datetime, duration FROM calls "
                           "WHERE datetime BETWEEN '%1' and '%2' AND "
                           "(seg = 'M' AND seg != 'J' OR numout NOT LIKE '7%' "
                           "OR numout NOT LIKE '0%') "
                           "ORDER BY datetime "
                           ";").arg(strFirstDateTime).arg(strLastDateTime);
    }

    QVector<double> vecConnect;
    QVector<double> vecDateTime;
    QVector<double> vecConnectChannel;
    QSqlQuery query = QSqlQuery(strQuery);
    QMultiMap<QDateTime, qint64> dtime;
    while (query.next()) {
//        vecConnect.append(query.value(1).toDouble());
//        vecDateTime.append(query.value(0).toDateTime().toTime_t());
//        vecConnectChannel.append(countChannel);
//        qDebug() << query.value(0).toDateTime().toString() << "\t"
//                 << query.value(1).toTime().toString() << "\t"
//                 << query.value(1).toTime().secsTo(QTime(0, 0, 0)) << "\t"
//                 << query.value(0).toDateTime().addSecs(- query.value(1).toTime().secsTo(QTime(0, 0, 0))).toString()
//                 << query.value(0).toDateTime().time().toString();
                    ;
        dtime.insert(query.value(0).toDateTime(),
                     query.value(1).toTime().secsTo(QTime(0, 0, 0)));
    }

    auto iter = dtime.constBegin();
    QDateTime currentDateTime = iter.key();
    uint countConnect = 0;
    QList<int> listTransition;
    QList<int> listTemp;
    while (iter != dtime.constEnd()) {
        if (iter.key() != currentDateTime) {
            countConnect += listTransition.size();
            vecDateTime.append(iter.key().toTime_t());
            vecConnect.append(countConnect);
            vecConnectChannel.append(countChannel);
            currentDateTime = iter.key();
//            qDebug() << countConnect << "\t"
//                     << listTransition.size() << "\t"
//                     << listTransition << "\t"
//                     << listTemp
//                        ;

            for (auto &lst : listTransition)
                lst -= 1;
            listTransition.removeAll(0);
            listTransition += listTemp;
            listTemp.clear();
            countConnect = 0;
        }

        int count = qFloor(-iter.value() / 60.0 + 0.5);
        ++countConnect;
        if (count)
            listTemp << count;

//        qDebug() << iter.key().toString() << "\t"
//                 << count;
        ++iter;
    }

    customPlot->deleteLater();
    customPlot = new QCustomPlot;
    vLayout->addWidget(customPlot);
    //
    double firstTime = firstDateTime->dateTime().toTime_t();
    double lastTime  = lastDateTime->dateTime().toTime_t();
    customPlot->legend->setVisible(true);
//    QFont legendFont = font();
//    legendFont.setPointSize(9);
//    customPlot->legend->setFont(legendFont);
    customPlot->legend->setBrush(QBrush(QColor(255,255,255,230)));
    customPlot->addGraph();
    customPlot->graph(0)->setData(vecDateTime, vecConnect);
//    customPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 3));
    customPlot->graph(0)->setName(tr("Загрузка СЛ"));
    customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    customPlot->xAxis->setDateTimeFormat("yyyy-MM-dd hh:mm");
//    customPlot->xAxis->setAutoTickStep(false);
//    customPlot->xAxis->setTickStep(60);
    customPlot->xAxis->setTickLabelRotation(45);
    customPlot->xAxis->setRange(firstTime, lastTime);
    customPlot->yAxis->setRange(0, rangeChannel) ;
    //
    customPlot->replot();
    customPlot->addGraph();
    customPlot->graph(1)->setData(vecDateTime, vecConnectChannel);
    customPlot->graph(1)->setName(tr("Максимально возможная загрузка СЛ"));
//    customPlot->graph(1)->setPen(QPen(Qt::red));
//    QPen pen = QPen(Qt::red);
    QPen pen = QPen((QColor(255, 100, 0)));
    pen.setWidth(2);
    pen.setStyle(Qt::DashLine);
    customPlot->graph(1)->setPen(pen);
    customPlot->graph(1)->setLineStyle(QCPGraph::lsLine);

    // add title layout element:
    customPlot->plotLayout()->insertRow(0);
    customPlot->plotLayout()->addElement(0, 0, new QCPPlotTitle(customPlot, tr("Загрузка соединительных линий")));
    customPlot->plotLayout()->insertRow(1);
    customPlot->plotLayout()->addElement(1, 0, new QCPPlotTitle(customPlot, tr("Aastra MX-One -- %1").arg(line)));
    // set labels
//    customPlot->xAxis->setLabel(tr("Дата время"));
    customPlot->yAxis->setLabel(tr("Количество соединений"));

    customPlot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    customPlot->replot();
}

void LoadLine::slotSaveToFile()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save to file"),
                                 "", tr("Text files (*.png);;All files (*)"));
    customPlot->savePng(saveFileName);
}

void LoadLine::createWidgets()
{
    customPlot = new QCustomPlot;

    firstDateTime = new QDateTimeEdit(QDateTime::currentDateTime().addSecs(-86400));
    firstDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");
    lastDateTime  = new QDateTimeEdit(QDateTime::currentDateTime());
    lastDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");

    comboLine = new QComboBox;
    comboLine->addItems(QStringList() << tr("Спорт-отель")
                                      << tr("Ericsson MD-110")
                                      << tr("Right-Fax")
                                      << tr("AXE-10 (Саяногорск)")
                        );

    QFormLayout *dateLayout = new QFormLayout;
    dateLayout->addRow(tr("Начало периода:"), firstDateTime);
    dateLayout->addRow(tr("Конец периода:"), lastDateTime);
    dateLayout->addRow(tr("Проверка потока:"), comboLine);

    vLayout = new QVBoxLayout;
    vLayout->addLayout(dateLayout);
    vLayout->addWidget(customPlot);

    QWidget *wgt = new QWidget;
    wgt->setLayout(vLayout);
    setCentralWidget(wgt);
}

