#ifndef LOADLINE_H
#define LOADLINE_H

#include "abstractcalls.h"

//class QLabel;
class QDateTimeEdit;
class QComboBox;
class QCustomPlot;
class QVBoxLayout;

class LoadLine : public AbstractCalls
{
    Q_OBJECT
public:
    explicit LoadLine(QWidget *parent = 0);
    ~LoadLine();

signals:

public slots:

private slots:
    void slotShowGraph(const QString &line);
    void slotSaveToFile();

private:
    void createWidgets();

private:
    QVBoxLayout *vLayout;
    QDateTimeEdit *firstDateTime;
    QDateTimeEdit *lastDateTime;
    QComboBox     *comboLine;
    QCustomPlot   *customPlot;
};

#endif // LOADLINE_H
