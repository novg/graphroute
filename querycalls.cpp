#include "querycalls.h"

#include <QDebug>
#include <QToolBar>
#include <QTableView>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QBoxLayout>
#include <QSplitter>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QItemSelectionModel>
#include <QShortcut>
#include <QClipboard>
#include <QApplication>

QueryCalls::QueryCalls(QWidget *parent) : AbstractCalls(parent)
{
    createActions();
    createWidgets();
    createConnect();

    connect(pushQuery, SIGNAL(clicked()), SLOT(slotShowTables()));
    connect(copyToClipboard, SIGNAL(activated()), SLOT(slotCopyToClipboard()));
}

QueryCalls::~QueryCalls()
{

}

void QueryCalls::createWidgets()
{
    tableView            = new QTableView;
    labelRowsCount       = new QLabel(tr("Всего вызовов: "));
    errorEdit            = new QLineEdit;
    pushQuery            = new QPushButton(tr("Выполнить запрос"));
    editQuery            = new QTextEdit;
    QLabel *labelQuery   = new QLabel(tr("SQL запрос:"));
    QLabel *errorSql     = new QLabel(tr("Ошибка базы данных:"));
    QWidget *widgetQuery = new QWidget;
    QWidget *widgetTable = new QWidget;

    //
    QHBoxLayout *pushLayout = new QHBoxLayout;
    pushLayout->addWidget(pushQuery);
    pushLayout->addStretch(1);

    //
    QVBoxLayout *vertLayout = new QVBoxLayout;
    vertLayout->addWidget(labelQuery);
    vertLayout->addWidget(editQuery);
    vertLayout->addLayout(pushLayout);
    vertLayout->addWidget(errorSql);
    vertLayout->addWidget(errorEdit);
    widgetQuery->setLayout(vertLayout);

    //
    QVBoxLayout *tableLayout = new QVBoxLayout;
    tableLayout->addWidget(tableView);
    tableLayout->addWidget(labelRowsCount);
    widgetTable->setLayout(tableLayout);

    //
    QSplitter *splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(widgetQuery);
    splitter->addWidget(widgetTable);
    splitter->setStretchFactor(0, 1);
    splitter->setStretchFactor(1, 10);
    setCentralWidget(splitter);

    copyToClipboard = new QShortcut(QKeySequence("Ctrl+C"),this);
}

void QueryCalls::slotShowTables()
{
    qmodel = new QSqlQueryModel(this);
    QString strQuery = editQuery->toPlainText();
    qmodel->setQuery(strQuery);

    if (qmodel->lastError().isValid()) {
        qDebug() << qmodel->lastError().text();
        errorEdit->setText(qmodel->lastError().text());
    } else {
        errorEdit->clear();
    }

    tableView->setModel(qmodel);
    tableView->resizeColumnsToContents();
    tableView->resizeRowsToContents();

    selection = new QItemSelectionModel(qmodel);
    tableView->setSelectionModel(selection);

    if (strQuery.contains(QRegExp((";\\s*$")))) {
        strQuery.remove(QRegExp(";\\s*$"));
    }
    strQuery = "(" + strQuery + ")";

    labelRowsCount->setText(tr("Всего вызовов: %1")
                            .arg(qmodel->rowCount()));
}

void QueryCalls::slotCopyToClipboard()
{
    QClipboard *clipboard = QApplication::clipboard();
    QModelIndexList listIndex = selection->selectedIndexes();
    QString strClipboard;
    int currentRow = listIndex.at(0).row();
    for (QModelIndex index : listIndex) {
        if (currentRow == index.row()) {
            strClipboard += "\t";
        } else {
            strClipboard += "\n";
            currentRow = index.row();
        }
        strClipboard += qmodel->data(index).toString();
    }

    strClipboard.remove(0,1);
    clipboard->setText(strClipboard);
}

