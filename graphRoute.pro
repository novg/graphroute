#-------------------------------------------------
#
# Project created by QtCreator 2015-02-22T14:07:38
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = graphRoute
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    settingsdialog.cpp \
    filtercalls.cpp \
    querycalls.cpp \
    abstractcalls.cpp \
    loadline.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    settingsdialog.h \
    filtercalls.h \
    querycalls.h \
    abstractcalls.h \
    loadline.h \
    qcustomplot.h

CONFIG += c++14

RC_FILE = icon.rc

RESOURCES += \
    images.qrc

DISTFILES +=
