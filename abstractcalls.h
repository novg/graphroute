#ifndef ABSTRACTCALLS_H
#define ABSTRACTCALLS_H

#include <QMainWindow>

class AbstractCalls : public QMainWindow
{
    Q_OBJECT
public:
    explicit AbstractCalls(QWidget *parent = 0);
    ~AbstractCalls();

signals:
    void signalSettings();
    void signalAbout();

protected:
    virtual void createWidgets() = 0;
    virtual void createActions();
    virtual void createConnect();

protected:
    QAction *actionSettings;
    QAction *actionSave;
    QAction *actionAbout;

public slots:
};

#endif // ABSTRACTCALLS_H
