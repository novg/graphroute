#ifndef FILTERCALLS_H
#define FILTERCALLS_H

#include "abstractcalls.h"

class QLabel;
class QTableView;
class QDateTimeEdit;
class QLineEdit;
class QSqlQueryModel;
class QSqlDatabase;

class FilterCalls : public AbstractCalls
{
    Q_OBJECT
public:
    explicit FilterCalls(QWidget *parent = 0);
    ~FilterCalls();

signals:

public slots:
    void slotSetTableName(const QString &name);

private slots:
    void slotShowTables();
    void slotSaveToFile();

private:
    void createWidgets();
    QString addQuotes(const QString &str);
    QString printLine(int length) const;
    QString delimeter(int column, int len) const;

private:
    QLabel *labelRowsCount;
    QTableView *tableView;
    QDateTimeEdit *firstDateTime;
    QDateTimeEdit *lastDateTime;
    QLineEdit *inNumber;
    QLineEdit *outNumber;
    QString tableName;
    QSqlQueryModel *qmodel;
};

#endif // FILTERCALLS_H
