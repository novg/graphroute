#include "abstractcalls.h"

#include <QAction>
#include <QToolBar>
#include <QDebug>

AbstractCalls::AbstractCalls(QWidget *parent) : QMainWindow(parent)
{
//    createActions();
}

AbstractCalls::~AbstractCalls()
{

}

void AbstractCalls::createActions()
{
    actionSettings = new QAction(tr("Настройки соединения"), this);
    actionSettings->setToolTip(tr("Настройки соединения"));
    actionSettings->setStatusTip(tr("Настройки соединения"));
    actionSettings->setWhatsThis(tr("Настройки соединения"));
    actionSettings->setIcon(QIcon(":/images/cog-icon-2-48x48.png"));

    actionSave = new QAction(tr("Сохранить в файл"), this);
    actionSave->setToolTip(tr("Сохранить в файл"));
    actionSave->setStatusTip(tr("Сохранить в файл"));
    actionSave->setWhatsThis(tr("Сохранить в файл"));
    actionSave->setIcon(QIcon(":/images/floppy.png"));

    actionAbout = new QAction(tr("О программе"), this);
    actionAbout->setToolTip(tr("О программе"));
    actionAbout->setStatusTip(tr("О программе"));
    actionAbout->setWhatsThis(tr("О программе"));
    actionAbout->setIcon(QIcon(":/images/about.png"));

    QList<QAction *> actions;
    actions << actionSettings << actionSave << actionAbout;

    QToolBar *toolBar;
    toolBar = addToolBar(tr("Панель настроек"));
    toolBar->addActions(actions);
}

void AbstractCalls::createConnect()
{
    connect(actionAbout, SIGNAL(triggered()), SIGNAL(signalAbout()));
    connect(actionSettings, SIGNAL(triggered()), SIGNAL(signalSettings()));
}

