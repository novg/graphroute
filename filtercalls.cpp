#include "filtercalls.h"

#include <QDebug>
#include <QLabel>
#include <QTableView>
#include <QDateTimeEdit>
#include <QValidator>
#include <QBoxLayout>
#include <QLineEdit>
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QFileDialog>
#include <QHeaderView>

FilterCalls::FilterCalls(QWidget *parent) : AbstractCalls(parent)
{
    createActions();
    createWidgets();
    createConnect();

    connect(firstDateTime, SIGNAL(editingFinished()), SLOT(slotShowTables()));
    connect(lastDateTime, SIGNAL(editingFinished()), SLOT(slotShowTables()));
    connect(inNumber, SIGNAL(returnPressed()), SLOT(slotShowTables()));
    connect(outNumber, SIGNAL(returnPressed()), SLOT(slotShowTables()));
    connect(actionSave, SIGNAL(triggered()), SLOT(slotSaveToFile()));
}

FilterCalls::~FilterCalls()
{

}

void FilterCalls::slotSetTableName(const QString &name)
{
    tableName = name;
}

void FilterCalls::slotShowTables()
{
    if (tableName.isEmpty())
        return;

    qmodel = new QSqlQueryModel(this);
    QString strDirIn  = inNumber->text();
    QString strDirOut = outNumber->text();
    QString strFirstDateTime =
            firstDateTime->dateTime().toString("yyyy-MM-dd hh:mm");
    QString strLastDateTime =
            lastDateTime->dateTime().toString("yyyy-MM-dd hh:mm");
    QString strQuery;

    if (!strDirIn.isEmpty())
        strDirIn = addQuotes(strDirIn);
    if (!strDirOut.isEmpty())
        strDirOut = addQuotes(strDirOut);

    if (strDirIn.isEmpty() and strDirOut.isEmpty()) {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3';"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                ;
    } else if (!strDirIn.isEmpty() and strDirOut.isEmpty()) {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numin IN (%4);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                ;
    } else if (strDirIn.isEmpty() and !strDirOut.isEmpty()) {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numout IN (%4);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirOut)
                ;
    } else {
        strQuery = QString("SELECT datetime, duration, numin, numout FROM %1 "
                           "WHERE datetime "
                           "BETWEEN '%2' AND '%3' "
                           "AND numin IN (%4) AND numout IN (%5);"
                           )
                .arg(tableName)
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                .arg(strDirOut)
                ;
    }

    qmodel->setQuery(strQuery);
    labelRowsCount->setText(tr("Всего вызовов: %1")
                            .arg(qmodel->rowCount()));

    if (qmodel->lastError().isValid())
        qDebug() << qmodel->lastError().text();

    qmodel->setHeaderData(0, Qt::Horizontal, "Дата время");
    qmodel->setHeaderData(1, Qt::Horizontal, "Длительность вызова");
    qmodel->setHeaderData(2, Qt::Horizontal, "Набираемый номер");
    qmodel->setHeaderData(3, Qt::Horizontal, "Вызывающий номер");
    tableView->setModel(qmodel);
    tableView->resizeColumnsToContents();
    tableView->resizeRowsToContents();
}

void FilterCalls::slotSaveToFile()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save to file"),
                                 "", tr("Text files (*.txt);;All files (*)"));
    if (saveFileName != "") {
        QFile saveFile(saveFileName);
        if (saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream stream(&saveFile);
            int length = 74;
            stream << printLine(length);
            stream << tr("Дата\t\tВремя") << "\t\t"
                   << tr("Длительность")  << "\t"
                   << tr("Набираемый")    << "\t"
                   << tr("Вызывающий")    << "\n"
                      ;
            stream << tr("вызова")
                   << tr("\t\tвызова")
                   << tr("\t\tвызова")
                   << tr("\t\tномер")
                   << tr("\t\tномер")
                   << "\n";
            stream << printLine(length);

            while (qmodel->canFetchMore())
                qmodel->fetchMore();

            int rowsCount   = tableView->verticalHeader()->count();
            int columnCount = tableView->horizontalHeader()->count();

            QString line;
            for (int row = 0; row < rowsCount; ++row) {
                for (int col = 0; col < columnCount; ++col) {
                    QModelIndex index = qmodel->index(row, col, QModelIndex());
                    line = qmodel->data(index).toString();
                    stream << line.replace("T", "\t")
                           << delimeter(col, line.length());
                }
            }

            stream << printLine(length);
            saveFile.close();
        }
    }
}

void FilterCalls::createWidgets()
{
    QWidget *widgetPanel     = new QWidget;
    QLabel  *labelFirstDate  = new QLabel(tr("Начало периода:"));
    QLabel  *labelLastDate   = new QLabel(tr("Конец периода:"));
    QLabel  *labelInNumber   = new QLabel(tr("Набираемый номер:"));
    QLabel  *labelOutNumber  = new QLabel(tr("Вызывающий номер:"));
    QWidget *widgetCentral   = new QWidget;
    labelRowsCount = new QLabel(tr("Всего вызовов: "));

    tableView     = new QTableView;
    firstDateTime = new QDateTimeEdit(QDateTime::currentDateTime().addSecs(-300));
    firstDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");
    lastDateTime  = new QDateTimeEdit(QDateTime::currentDateTime());
    lastDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");
    inNumber      = new QLineEdit;
    outNumber     = new QLineEdit;

    QValidator *validator = new QRegExpValidator(QRegExp("[0-9,]*"));
    inNumber->setValidator(validator);
    outNumber->setValidator(validator);

    // Layout setup
    QVBoxLayout *inNumberLayout  = new QVBoxLayout;
    inNumberLayout->addWidget(labelInNumber);
    inNumberLayout->addWidget(inNumber);

    QVBoxLayout *outNumberLayout = new QVBoxLayout;
    outNumberLayout->addWidget(labelOutNumber);
    outNumberLayout->addWidget(outNumber);

    QVBoxLayout *firstDateLayout = new QVBoxLayout;
    firstDateLayout->addWidget(labelFirstDate);
    firstDateLayout->addWidget(firstDateTime);

    QVBoxLayout *lastDateLayout  = new QVBoxLayout;
    lastDateLayout->addWidget(labelLastDate);
    lastDateLayout->addWidget(lastDateTime);

    QHBoxLayout *groupLayout = new QHBoxLayout;
    groupLayout->addLayout(firstDateLayout);
    groupLayout->addLayout(lastDateLayout);
    groupLayout->addStretch(1);
    groupLayout->addLayout(inNumberLayout);
    groupLayout->addLayout(outNumberLayout);
    widgetPanel->setLayout(groupLayout);

    QVBoxLayout *commonLayout    = new QVBoxLayout;
    commonLayout->addWidget(widgetPanel);
    commonLayout->addWidget(tableView);
    commonLayout->addWidget(labelRowsCount);
    widgetCentral->setLayout(commonLayout);

    setCentralWidget(widgetCentral);
}

QString FilterCalls::addQuotes(const QString &str)
{
    QStringList list = str.split(",");
    for (auto &iter : list)
        iter = "'" + iter + "'";

    return list.join(",");
}

QString FilterCalls::printLine(int length) const
{
    QString line;
    for (int i = 0; i < length; ++i)
        line += "-";

    line += "\n";
    return line;
}

QString FilterCalls::delimeter(int column, int len) const
{
    switch (column) {
    case 0:
        return "\t";
        break;
    case 1:
        return "\t";
        break;
    case 2:
        if (len > 5)
            return "\t";
        else
            return "\t\t";
        break;
    default:
        return "\n";
        break;
    }
}
