#ifndef QUERYCALLS_H
#define QUERYCALLS_H

#include "abstractcalls.h"

class QTableView;
class QLabel;
class QLineEdit;
class QPushButton;
class QTextEdit;
class QSqlQueryModel;
class QShortcut;
class QItemSelectionModel;

class QueryCalls : public AbstractCalls
{
    Q_OBJECT
public:
    explicit QueryCalls(QWidget *parent = 0);
    ~QueryCalls();

signals:

private slots:
    void slotShowTables();
    void slotCopyToClipboard();

private:
    void createWidgets();

private:
    QTableView *tableView;
    QLabel *labelRowsCount;
    QLineEdit *errorEdit;
    QPushButton *pushQuery;
    QTextEdit *editQuery;
    QSqlQueryModel *qmodel;
    QShortcut *copyToClipboard;
    QItemSelectionModel *selection;
};

#endif // QUERYCALLS_H
