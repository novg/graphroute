#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString user = qgetenv("USERNAME");
    if (user != "NovgorodskiyAK")
        return 0;

    MainWindow w;
    w.resize(600, 600);
    w.show();
    return a.exec();
}
