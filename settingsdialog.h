#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

class QLineEdit;

class SettingsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

public:
    QString host() const;
    QString base() const;
    QString user() const;
    QString password() const;

signals:

public slots:

private:
    void readSettings();

private:
    QLineEdit *lineEditHost;
    QLineEdit *lineEditBase;
    QLineEdit *lineEditUser;
    QLineEdit *lineEditPassword;
    QString hostName;
    QString baseName;
    QString userName;
    QString passwrd;
};

#endif // SETTINGSDIALOG_H
