#include "mainwindow.h"
#include "settingsdialog.h"
#include "filtercalls.h"
//#include "querycalls.h"
#include "loadline.h"

#include <QDebug>
#include <QTabWidget>
#include <QSqlDatabase>
#include <QSqlError>
#include <QLabel>
#include <QTableView>
#include <QDateTimeEdit>
#include <QLineEdit>
#include <QValidator>
#include <QBoxLayout>
#include <QAction>
#include <QToolBar>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>
#include <QHeaderView>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    readSettings();
    createWidgets();

    connect(this, SIGNAL(signalSendTableName(QString)),
            filterCalls, SLOT(slotSetTableName(QString)));

    connect(filterCalls, SIGNAL(signalSettings()), SLOT(slotSettings()));
//    connect(queryCalls, SIGNAL(signalSettings()), SLOT(slotSettings()));
    connect(loadLine, SIGNAL(signalSettings()), SLOT(slotSettings()));
    connect(filterCalls, SIGNAL(signalAbout()), SLOT(slotAbout()));
//    connect(queryCalls, SIGNAL(signalAbout()), SLOT(slotAbout()));
    connect(loadLine, SIGNAL(signalAbout()), SLOT(slotAbout()));

    createConnection();
}

MainWindow::~MainWindow()
{
}

void MainWindow::slotSettings()
{
    SettingsDialog *settingsDialog = new SettingsDialog;
    if (settingsDialog->exec() == QDialog::Accepted) {
        hostName = settingsDialog->host();
        baseName = settingsDialog->base();
        userName = settingsDialog->user();
        password = settingsDialog->password();
        writeSettings();
        createConnection();
    }

    settingsDialog->deleteLater();
}

void MainWindow::slotAbout()
{
    QMessageBox::about(0, "About", "<h1><b><i><u>clientData ver. 0.8</u></i></b></h1>"
                       "produced by: A. Novgorodskiy<br/>"
                       "email: <a href=\"mailto:novg.novg@gmail.com\">novg.novg@gmail.com</a><br/>"
                       );
}

void MainWindow::readSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectSQL");
    hostName = settings.value("/host").toString();
    baseName = settings.value("/base").toString();
    userName = settings.value("/user").toString();
    password = settings.value("/password").toString();
    settings.endGroup();
}

void MainWindow::writeSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectSQL");
    settings.setValue("/host", hostName);
    settings.setValue("/base", baseName);
    settings.setValue("/user", userName);
    settings.setValue("/password", password);
    settings.endGroup();
}

bool MainWindow::createConnection()
{
    QSqlDatabase db;
    if(QSqlDatabase::contains(QSqlDatabase::defaultConnection))
        db = QSqlDatabase::database();
    else
        db = QSqlDatabase::addDatabase("QPSQL");

    db.setHostName(hostName);
//    db.setPort(5432);
    db.setDatabaseName(baseName);
    db.setUserName(userName);
    db.setPassword(password);

    if (!db.open()) {
        QMessageBox::critical(0, tr("Ошибка"), tr("Невозможно открыть базу данных"),
                              QMessageBox::Ok);
        return false;
    }

    QString tableName = db.tables(QSql::Tables)[0];
    emit signalSendTableName(tableName);

    return true;
}

void MainWindow::createWidgets()
{
    QTabWidget *tabWidget = new QTabWidget(this);
    filterCalls = new FilterCalls;
//    queryCalls = new QueryCalls;
    loadLine = new LoadLine;
    tabWidget->addTab(filterCalls, tr("Вызовы"));
//    tabWidget->addTab(queryCalls, tr("Запрос"));
    tabWidget->addTab(loadLine, tr("Нагрузка"));
    setCentralWidget(tabWidget);
}
