#include "settingsdialog.h"

#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QSettings>

SettingsDialog::SettingsDialog(QWidget *parent)
    : QDialog(parent, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
    readSettings();

    lineEditHost = new QLineEdit;
    lineEditHost->setText(hostName);

    lineEditBase = new QLineEdit;
    lineEditBase->setText(baseName);

    lineEditUser = new QLineEdit;
    lineEditUser->setText(userName);

    lineEditPassword = new QLineEdit;
    lineEditPassword->setText(passwrd);

    QLabel *labelHost = new QLabel("Host name:");
    QLabel *labelBase = new QLabel("Base name:");
    QLabel *labelUser = new QLabel("User name:");
    QLabel *labelPassword = new QLabel("Password:");

    QPushButton *buttonOk = new QPushButton("&OK");
    QPushButton *buttonCancel = new QPushButton("&Cancel");

    connect(buttonOk, SIGNAL(clicked()), SLOT(accept()));
    connect(buttonCancel, SIGNAL(clicked()), SLOT(reject()));

    QGridLayout *gridLayout = new QGridLayout;
    gridLayout->addWidget(labelHost, 0, 0);
    gridLayout->addWidget(lineEditHost, 0, 1);
    gridLayout->addWidget(labelBase, 1, 0);
    gridLayout->addWidget(lineEditBase, 1, 1);
    gridLayout->addWidget(labelUser, 2, 0);
    gridLayout->addWidget(lineEditUser, 2, 1);
    gridLayout->addWidget(labelPassword, 3, 0);
    gridLayout->addWidget(lineEditPassword, 3, 1);
    gridLayout->addWidget(buttonOk, 4, 0);
    gridLayout->addWidget(buttonCancel, 4, 1);
    setLayout(gridLayout);
}

SettingsDialog::~SettingsDialog()
{
}

QString SettingsDialog::host() const
{
    return lineEditHost->text();
}

QString SettingsDialog::base() const
{
    return lineEditBase->text();
}

QString SettingsDialog::user() const
{
    return lineEditUser->text();
}

QString SettingsDialog::password() const
{
    return lineEditPassword->text();
}

void SettingsDialog::readSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectSQL");
    hostName = settings.value("/host").toString();
    baseName = settings.value("/base").toString();
    userName = settings.value("/user").toString();
    passwrd = settings.value("/password").toString();
    settings.endGroup();
}
